-- Organizations 
INSERT INTO `quartz`.`organisations` (`id`, `name`, `create_date`, `organisation_type`) VALUES (1, 'Adidas', '2016-10-21 09:00:00', 2);
INSERT INTO `quartz`.`organisations` (`id`, `name`, `create_date`, `organisation_type`) VALUES (2, 'Nike', '2016-10-21 09:00:00', 2);
INSERT INTO `quartz`.`organisations` (`id`, `name`, `create_date`, `organisation_type`) VALUES (3, 'Loto', '2016-10-21 09:00:00', 2);
INSERT INTO `quartz`.`organisations` (`id`, `name`, `create_date`, `organisation_type`) VALUES (4, 'Puma', '2016-10-21 09:00:00', 2);
INSERT INTO `quartz`.`organisations` (`id`, `name`, `create_date`, `organisation_type`) VALUES (5, 'Replay', '2016-10-21 09:00:00', 2);
INSERT INTO `quartz`.`organisations` (`id`, `name`, `create_date`, `organisation_type`) VALUES (6, 'H&M', '2016-10-21 09:00:00', 2);
INSERT INTO `quartz`.`organisations` (`id`, `name`, `create_date`, `organisation_type`) VALUES (7, 'Zara', '2016-10-21 09:00:00', 2);

-- Contacts
INSERT INTO `quartz`.`contacts` (`id`, `title`, `first_name`, `last_name`, `position`, `email`, `mobile`, `DDI`, `create_date`, `status`, `organisation_id`)
VALUES (1, 'Mr.', 'Tiago', 'Silva', 'Software Developer', 'tiago@teamapet.com', '+440000000000','112345646', '2016-10-21 09:00:00', 1, 1);
INSERT INTO `quartz`.`contacts` (`id`, `title`, `first_name`, `last_name`, `position`, `email`, `mobile`, `DDI`, `create_date`, `status`, `organisation_id`)
VALUES (2, 'Mr.', 'Jonas', 'Vasiliauskas', 'Software Developer', 'jonas@teamapet.com', '+440000000000','112345646', '2016-10-21 09:00:00', 1, 2);
INSERT INTO `quartz`.`contacts` (`id`, `title`, `first_name`, `last_name`, `position`, `email`, `mobile`, `DDI`, `create_date`, `status`, `organisation_id`)
VALUES (3, 'Mr.', 'Richard', 'Maytson', 'Software Developer', 'richard@teamapet.com', '+440000000000','112345646', '2016-10-21 09:00:00', 1, 3);
INSERT INTO `quartz`.`contacts` (`id`, `title`, `first_name`, `last_name`, `position`, `email`, `mobile`, `DDI`, `create_date`, `status`, `organisation_id`)
VALUES (4, 'Mr.', 'Andrew', 'Elia', 'Software Developer', 'andrew@teamapet.com', '+440000000000','112345646', '2016-10-21 09:00:00', 1, 4);
INSERT INTO `quartz`.`contacts` (`id`, `title`, `first_name`, `last_name`, `position`, `email`, `mobile`, `DDI`, `create_date`, `status`, `organisation_id`)
VALUES (5, 'Mrs.', 'Melanie', 'Eggers', 'Software Developer', 'melanie@teamapet.com', '+440000000000','112345646', '2016-10-21 09:00:00', 1, 5);
INSERT INTO `quartz`.`contacts` (`id`, `title`, `first_name`, `last_name`, `position`, `email`, `mobile`, `DDI`, `create_date`, `status`, `organisation_id`)
VALUES (6, 'Mr.', 'Si', 'Parker', 'Software Developer', 'si@teamapet.com', '+440000000000','112345646', '2016-10-21 09:00:00', 1, 6);


INSERT INTO `quartz`.`users` (`id`,`access_level`,`status`,`last_login`,`password`,`passwd_change_date`,`passwd_fail_count`,`create_date`,`organisation_id`,`contact_id`)
VALUES(1, 2, 2, '2016-10-24 16:00:00','12456789', '2016-10-24 16:00:00', 0, '2016-10-24 16:00:00', 1, 1);
INSERT INTO `quartz`.`users` (`id`,`access_level`,`status`,`last_login`,`password`,`passwd_change_date`,`passwd_fail_count`,`create_date`,`organisation_id`,`contact_id`)
VALUES(2, 2, 2, '2016-10-24 16:00:00','12456789', '2016-10-24 16:00:00', 0, '2016-10-24 16:00:00', 2, 2);
INSERT INTO `quartz`.`users` (`id`,`access_level`,`status`,`last_login`,`password`,`passwd_change_date`,`passwd_fail_count`,`create_date`,`organisation_id`,`contact_id`)
VALUES(3, 2, 2, '2016-10-24 16:00:00', '12456789', '2016-10-24 16:00:00', 0, '2016-10-24 16:00:00', 3, 3);
INSERT INTO `quartz`.`users` (`id`,`access_level`,`status`,`last_login`,`password`,`passwd_change_date`,`passwd_fail_count`,`create_date`,`organisation_id`,`contact_id`)
VALUES(4, 2, 2, '2016-10-24 16:00:00','12456789', '2016-10-24 16:00:00', 0, '2016-10-24 16:00:00', 4, 4);
INSERT INTO `quartz`.`users` (`id`,`access_level`,`status`,`last_login`,`password`,`passwd_change_date`,`passwd_fail_count`,`create_date`,`organisation_id`,`contact_id`)
VALUES(5, 2, 2, '2016-10-24 16:00:00','12456789', '2016-10-24 16:00:00', 0, '2016-10-24 16:00:00', 5, 5);
INSERT INTO `quartz`.`users` (`id`,`access_level`,`status`,`last_login`,`password`,`passwd_change_date`,`passwd_fail_count`,`create_date`,`organisation_id`,`contact_id`)
VALUES(6, 2, 2, '2016-10-24 16:00:00','12456789', '2016-10-24 16:00:00', 0, '2016-10-24 16:00:00', 6, 6);
'''
Created on 23 Oct 2016

@author: tiago
'''

#  APIs and VIEWS
def register_api(app):
    BASE_URI = app.config['API_URL']

    from views.api import api_views
    app.register_blueprint(api_views, url_prefix = BASE_URI)
     
    from views.user import user_view
    app.register_blueprint(user_view, url_prefix = BASE_URI + user_view.name)
     
    from views.brand import brand_view
    app.register_blueprint(brand_view, url_prefix = BASE_URI + brand_view.name)
     
    from views.territory import territory_view
    app.register_blueprint(territory_view, url_prefix = BASE_URI + territory_view.name)
     
    from views.trademark_class import tradem_class_view
    app.register_blueprint(tradem_class_view, url_prefix = BASE_URI + tradem_class_view.name)
     
    from views.organisation import org_view
    app.register_blueprint(org_view, url_prefix = BASE_URI + org_view.name)
     
    from views.contact import contact_view
    app.register_blueprint(contact_view, url_prefix = BASE_URI + contact_view.name)
     
    from views.trademark import trademark_view
    app.register_blueprint(trademark_view, url_prefix = BASE_URI + trademark_view.name)
     
    from views.subscription import subscription_view
    app.register_blueprint(subscription_view, url_prefix = BASE_URI + subscription_view.name)
     
    from views.transaction import transaction_view
    app.register_blueprint(transaction_view, url_prefix = BASE_URI + transaction_view.name)
     
    from views.card_details import carddet_view
    app.register_blueprint(carddet_view, url_prefix = BASE_URI + carddet_view.name)
     
    from views.paypal_details import paypal_det_view
    app.register_blueprint(paypal_det_view, url_prefix = BASE_URI + paypal_det_view.name)
     
    from views.bank_details import bankdet_view
    app.register_blueprint(bankdet_view, url_prefix = BASE_URI + bankdet_view.name)
     

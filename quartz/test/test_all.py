'''
Link to all unit tests so they can be run from one .py file

Created on 29 Sep 2016

@author: si
'''
import unittest

from quartz.test.test_api import ApiViewsTest

if __name__ == "__main__":
    unittest.main()

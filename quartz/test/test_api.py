'''
Created on 20 Oct 2016

@author: tiago
'''
import unittest

from quartz.app import create_app
from quartz.database import db, create_database
from quartz.settings.test_config import Config

SHOW_LOG_MESSAGES = False

import logging
logging.disable(logging.ERROR)

class ApiViewsTest(unittest.TestCase):

    def setUp(self):

        self.config = Config(db_in_memory=False)
        self.app = create_app(self.config)
        self.test_client = self.app.test_client()

        with self.app.app_context():
            create_database(self.config)

    def tearDown(self):
        with self.app.app_context():
            db.drop_all()
        self.config.drop_db()

    def log(self, msg):
        if SHOW_LOG_MESSAGES:
            print msg

    def test_admin_root(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/')
        rv_post = self.test_client.post('/quartz/api/v1/')
        assert rv_get.status_code == 200
        assert rv_post.status_code == 200
        
    def test_level_users(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/users/')
        assert rv_get.status_code == 200
         
 
    def test_level_brands(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/brands/')
        assert rv_get.status_code == 200
    
    def test_level_territory(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/territories/')
        assert rv_get.status_code == 200
#         
    def test_level_trademark_class(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/trademark_classes/')
        assert rv_get.status_code == 200
        
    def test_level_organisation(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/organisations/')
        assert rv_get.status_code == 200
        
    def test_level_contact(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/contacts/')
        assert rv_get.status_code == 200
        
    def test_level_trademark(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/trademarks/')
        assert rv_get.status_code == 200
        
    def test_level_subscription(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/subscriptions/')
        assert rv_get.status_code == 200
        
    def test_level_transaction(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/transactions/')
        assert rv_get.status_code == 200
        
    def test_level_card_details(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/card_details/')
        assert rv_get.status_code == 200
        
    def test_level_paypal_details(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/paypal_details/')
        assert rv_get.status_code == 200
        
    def test_level_bank_details(self):
        """
        Return ok: 200
        """
        rv_get = self.test_client.get('/quartz/api/v1/bank_details/')
        assert rv_get.status_code == 200

if __name__ == "__main__":
    unittest.main()

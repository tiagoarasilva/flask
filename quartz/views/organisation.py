'''
Created on 21 Oct 2016

@author: tiago
'''
from flask import Blueprint, jsonify, current_app, request, make_response, render_template
from quartz.utils import JsonException
from quartz.database_models.dbmodels import Organisation

org_view = Blueprint(Organisation.__tablename__, __name__)

@org_view.route('/', methods=['GET', 'POST'])
def index():
    '''
    index - nothing to show
    '''
    return jsonify({})
'''
Created on 20 Oct 2016

@author: tiago
'''
from flask import Blueprint, current_app, request, abort, render_template
from quartz.utils import return_response_message

api_views = Blueprint('api', __name__)


@api_views.route('/', methods=['GET', 'POST'])
def index():
    """
    Index page
    """
    msg = 'The resource you are trying to access is not available at this level.'
    msg_dict = {'msg': msg, 'status_code': 200}
    current_app.logger.error(msg)
    
    if request.method == 'GET':
        return render_template('info_access.html', info = msg_dict)

    elif request.method == 'POST':
        return return_response_message(msg_dict, 200)
    
    else:
        abort(405)
'''
Created on 21 Oct 2016

@author: tiago
'''
from flask import Blueprint, jsonify, current_app, request, make_response
from quartz.utils import JsonException
from quartz.database_models.dbmodels import Contact

contact_view = Blueprint(Contact.__tablename__, __name__)

@contact_view.route('/')
def index():
    '''
    index - nothing to show
    '''
    return jsonify({})
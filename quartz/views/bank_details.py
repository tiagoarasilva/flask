'''
Created on 21 Oct 2016

@author: tiago
'''
from flask import Blueprint, jsonify, current_app, request, make_response
from quartz.utils import JsonException
from quartz.database_models.dbmodels import BankDetails

bankdet_view = Blueprint(BankDetails.__tablename__, __name__)

@bankdet_view.route('/')
def index():
    '''
    index - nothing to show
    '''
    return jsonify({})
'''
Created on 21 Oct 2016

@author: tiago
'''
import json
from flask import Blueprint, jsonify
from flask.views import MethodView
from quartz.utils import return_response_message
from quartz.database_models.dbmodels import User

user_view = Blueprint(User.__tablename__, __name__)

class UserApi(MethodView):
    '''
    With the flask.views.MethodView you can easily do that. 
    Each HTTP method maps to a function with the same name (just in lowercase):
    '''
    def get(self, user_id):
        '''
        Get a user or a list of users
        '''
        if user_id is None:
            # Returns the list of users
            users = User.query.all()
            u = json.dumps(users)
            return return_response_message(u, 200)
        else:
            return jsonify(user = 'User')
    
    def post(self):
        '''
        Create a new user
        '''
        return jsonify(erro = "POST")
        
        
    def put(self):
        '''
        Update a user
        '''
        return jsonify(erro = "PUT")
        
    def delete(self):
        '''
        Delete a user
        '''
        return jsonify(erro = "DELETE")

    
    @classmethod
    def register_api(cls, mod):
        url_view = cls.as_view('user_api')
        mod.add_url_rule('/user/' + '<int:user_id>', view_func=url_view, methods=['GET'])
        mod.add_url_rule('/register/', view_func=url_view, methods=['POST'])
        mod.add_url_rule('/update/', view_func=url_view, methods=['PUT'])
        mod.add_url_rule('/remove/', view_func=url_view, methods=['DELETE'])

UserApi.register_api(user_view)

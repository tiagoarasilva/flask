'''
Created on 21 Oct 2016

@author: tiago
'''
from sqlalchemy import MetaData, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from quartz.database import db
metadata = MetaData()


class User(db.Model):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    access_level = Column(Integer)
    status = Column(Integer)
    last_login = Column(DateTime)
    password = Column(String(20))
    passwd_change_date = Column(DateTime)
    passwd_fail_count = Column(Integer)
    create_date = Column(DateTime)
    organisation_id = Column(Integer, ForeignKey('organisations.id'))
    contact_id = Column(Integer, ForeignKey('contacts.id'))
    organisation = relationship("Organisation", backref="user")
    contact = relationship("Contact", backref="user")

    def __repr__(self):
        return '<User %r>' % (self.id)

class Brand(db.Model):
    __tablename__ = 'brands'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    logo = Column(String(100))
    create_date = Column(DateTime)
    organisation_id = Column(Integer, ForeignKey('organisations.id'))
    organisation = relationship("Organisation", backref="brand")

    def __repr__(self):
        return '<Brand %r>' % (self.name)

class Territory(db.Model):
    __tablename__ = 'territories'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    brand_id = Column(Integer, ForeignKey('brands.id'))
    organisation_id = Column(Integer, ForeignKey('organisations.id'))
    organisation = relationship("Organisation", backref="territory")
    createDate = Column(DateTime)

    def __repr__(self):
        return '<Territory %r>' % (self.name)

class TrademarkClass(db.Model):
    __tablename__ = 'trademark_classes'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    territory_id = Column(Integer, ForeignKey('territories.id'))
    territory = relationship("Territory", backref="trademarkclass")

    def __repr__(self):
        return '<TrademarkClass %r>' % (self.name)

class Organisation(db.Model):
    __tablename__ = 'organisations'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    create_date = Column(DateTime)
    organisation_type = Column(Integer)

    def __repr__(self):
        return '<Organisation %r>' % (self.name)

class Contact(db.Model):
    __tablename__ = 'contacts'
    id = Column(Integer, primary_key=True)
    title = Column(String(100))
    first_name = Column(String(100))
    last_name = Column(String(100))
    position = Column(String(100))
    email = Column(String(100), unique=True)
    mobile = Column(String(100))
    DDI = Column(String(100))
    create_date = Column(DateTime)
    status = Column(Integer)
    organisation_id = Column(Integer, ForeignKey('organisations.id'))
    organisation = relationship("Organisation", backref="contact")


    def __repr__(self):
        return '<Contact %r>' % (self.firstName + " " + self.lastName)

class Trademark(db.Model):
    __tablename__ = 'trademarks' 
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    number = Column(String(100))
    brand_id = Column(Integer, ForeignKey('brands.id'))
    brand = relationship("Brand", backref="trademark")

    def __repr__(self):
        return '<Trademark %r>' % (self.name)

class Subscription(db.Model):
    __tablename__ = 'subscriptions'
    id = Column(Integer, primary_key=True)
    expiry = Column(DateTime)
    status = Column(Integer)
    trademark_id = Column(Integer, ForeignKey('trademarks.id'))
    trademark = relationship("Trademark", backref="subscription")
    
    def __repr__(self):
        return '<Subscription %r>' % (self.id)

class Transaction(db.Model):
    __tablename__ = 'transactions'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    reference = Column(String(100))
    status = Column(Integer)
    transaction_type = Column(Integer)
    subscription_id = Column(Integer, ForeignKey('subscriptions.id'))
    subscription = relationship("Subscription", backref="transaction")

    def __repr__(self):
        return '<Transaction %r>' % (self.reference)

class CardDetails(db.Model):
    __tablename__ = 'card_details'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    number = Column(String(100))
    expiry = Column(DateTime)
    organisation_id = Column(Integer, ForeignKey('organisations.id'))
    organisation = relationship("Organisation", backref="carddetails")
    
    
    def __repr__(self):
        return '<CardDetails %r>' % (self.name)

class PayPalDetails(db.Model):
    __tablename__ = 'paypal_details'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    email = Column(String(100), unique=True)
    organisation_id = Column(Integer, ForeignKey('organisations.id'))
    organisation = relationship("Organisation", backref="paypaldetails")
    
    def __repr__(self):
        return '<PayPalDetails %r>' % (self.name)

class BankDetails(db.Model):
    __tablename__ = 'bank_details'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    IBAN = Column(String(100))
    sort_code = Column(String(100))
    account_number = Column(String(100))
    organisation_id = Column(Integer, ForeignKey('organisations.id'))
    organisation = relationship("Organisation", backref="bankdetails")
    
    def __repr__(self):
        return '<BankDetails %r>' % (self.name)

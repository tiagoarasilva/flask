'''
Created on 20 Oct 2016

@author: tiago
'''
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def create_database(config_module):

    from quartz.app import create_app
    app = create_app(config_module)
    
    with app.app_context():
        db.create_all()


if __name__ == '__main__':
    # input stdin from the command line
    if raw_input("Create database using local config? y/n ") == 'y':
        print "creating database..."
        
        # import all the models from the database_models
        from quartz.database_models.dbmodels import *
        create_database('settings.local_config.Config')
        print "done"
    else:
        print ("Error whilst creating the database. DB not created!")

'''
Created on 20 Oct 2016

@author: tiago
'''
from flask import Flask, jsonify
from quartz.database import db
from quartz.register_api import register_api


def create_app(settings_class):
    app = Flask(__name__)
    app.config.from_object(settings_class)
    
    db.init_app(app)
    
    # Generate the views
    register_api(app)

    from utils import JsonException
    
    # app wide JSON errors
    def handle_json_exception(error):
        if hasattr(error, 'to_dict') and hasattr(error, 'http_status_code'):
            response = jsonify(error.to_dict())
            response.status_code = error.http_status_code
            return response
        elif hasattr(error, 'message'):
            response = jsonify({'msg': error.message})
            return response, 500
        else:
            response = jsonify({'msg': 'Unknown error'})
            return response, 500
    app.register_error_handler(JsonException, handle_json_exception)
    
    return app


if __name__ == '__main__':
    app = create_app('settings.local_config.Config')
    app.run(debug=app.config['DEBUG'], use_reloader=False, port=5002)

'''
Created on 21 Oct 2016

@author: tiago
'''
from flask import current_app, make_response, jsonify, render_template
from flask.views import View

class JsonException(Exception):

    def __init__(self, message, http_status_code=400):
        """
        @param http_status_code: integer
        @param message: str
        """
        Exception.__init__(self)
        self.message = message
        self.http_status_code = http_status_code

        if http_status_code >= 400 and http_status_code != 404:
            current_app.logger.error(message)

    def to_dict(self):
        rv = { 'message' : self.message }
        return rv
    

class ListView(View):
    
    def get_template_name(self):
        raise NotImplementedError()
    
    def render_template(self, context):
        return render_template(self.get_template_name(), **context)
    
    def dispatch_request(self):
        context = {'objects': self.get_objects()}
        return self.render_template(context)


def return_response_message(obj_message, status):
    '''
    Handler for JSON responses
    @return: response
    '''
    rv = make_response(jsonify(result = obj_message), status)
    rv.mimetype = 'application/json'
    return rv
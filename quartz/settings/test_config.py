from global_config import BaseConfig

import os
import tempfile

class Config(BaseConfig):
    APP_TITLE = BaseConfig.APP_TITLE+" - Testing"
    DEBUG = True
    TESTING = True
    DB_FILE = None # changed to a temp file before use
    DB_FD = None
    SQLALCHEMY_DATABASE_URI = None
    
    # API URL SETTINGS
    API_VERSION = "/v1/"
    API_URL = BaseConfig.API_PREFIX + API_VERSION
    
    def __init__(self, db_in_memory=True):
        self.db_in_memory = db_in_memory
        if self.SQLALCHEMY_DATABASE_URI == None:
            if self.db_in_memory:
                self.SQLALCHEMY_DATABASE_URI = "sqlite://"
            else:
                self.DB_FD, self.DB_FILE = tempfile.mkstemp()
                self.SQLALCHEMY_DATABASE_URI = "sqlite:///%s" % self.DB_FILE

        if not self.db_in_memory and False:
            print "using db [%s]" % self.SQLALCHEMY_DATABASE_URI

    def drop_db(self):
        if not self.db_in_memory and self.DB_FILE:
            os.close(self.DB_FD)
            #print "deleting %s" % self.DB_FILE
            os.unlink(self.DB_FILE)

from global_config import BaseConfig

class Config(BaseConfig):
    DEBUG = True
    API_VERSION = ""
    SQLALCHEMY_DATABASE_URI = "mysql://root:supersecret@localhost/quartz"
    SECRET_KEY = "451e1f362c5957"

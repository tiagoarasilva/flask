class BaseConfig(object):
    DEBUG = False
    APP_TITLE = "Quartz"
    API_PREFIX = "/quartz/api"
    API_VERSION = ""
    SQLALCHEMY_DATABASE_URI = ""
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = ""
    SQLALCHEMY_DATABASE_URI = "" # really important

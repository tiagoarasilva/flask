# Quartz #

##  Schema with RESTful Access - CRUD ##

RESTful API and database models


## Implementation Notes 

* It's a [Flask](http://flask.pocoo.org/) app with a [Flask-SQLAlchemy](https://pythonhosted.org/Flask-SQLAlchemy/) managed database.
* Please install first the requirements needed. All are located in requirements.txt

## Module Installation Note:

* For the module pymysql, you need to change your SQLALCHEMY_DATABASE_URI to `SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://.....'`


## Getting Started - Unit tests

```Shell
[tiago@teamapet Quartz]$ export PYTHONPATH=`pwd`
[tiago@teamapet Quartz]$ cd quartz/tests/
[tiago@teamapet tests]$ python test_all.py

----------------------------------------------------------------------
Ran 13 tests in 1.471s

OK

```

## Getting Started - Running locally

Create the empty database (permissions are not shown here)-
```SQL
create database quartz;
```

## Configuration Settings

Create your own settings file and symbolic link to the manage command-
```Shell
[tiago@teamapet Quartz]$ cd quartz/settings/
[tiago@teamapet settings]$ cp example_config.py local_config_ti.py 
[tiago@teamapet settings]$ ln -s local_config_ti.py local_config.py
```

## Start Application

Then run it-
[tiago@teamapet Quartz]$ export PYTHONPATH=`pwd`
[tiago@teamapet Quartz]$ cd quartz/
[tiago@teamapet quartz]$ python app.py
 * Running on http://127.0.0.1:5001/